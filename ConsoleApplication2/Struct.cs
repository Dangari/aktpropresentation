﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation
{
    struct  Vec4<T>
    {
        public T x;
        public T y;
        public T z;
        public T q;

        public Vec4(T x, T y, T z, T q)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.q = q;
        }
    }
}
