﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation
{
    class OutRef
    {
        public int addOne(int a)
        {
            a++;
            return a;
        }

        public void addOne(ref int a)
        {
            a++;
        }

        public void fillArray(out int[] table, int size) 
        {
            table = new int[size];

            Random rnd = new Random();
            for(int i = 0; i< table.Length; i++)
            {
                table[i] = rnd.Next(1, 14);
            }
        }

    }
}
