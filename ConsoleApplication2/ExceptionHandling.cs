﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation
{
    class NodeNotFoundException : System.Exception
    {
        public NodeNotFoundException() : base() { }
        public NodeNotFoundException(string message) : base(message) { }
    }

    class Node
    {
        public Node left;
        public Node right;
        public int value;

        public Node(int value, Node left = null, Node right = null)
        {
            this.left = left;
            this.right = right;
            this.value = value;
        }
    }

    class BinTree
    {
        private Node startNode;

        public BinTree(Node startNode = null)
        {
            this.startNode = startNode;
        }

        public void addNode(int x)
        {
            if (startNode == null)
            {
                startNode = new Node(x);
            }
            else
            {
                bool stop = false;
                Node n = startNode;
                while (stop == false)
                {
                    if (n.value > x)
                    {
                        if (n.left == null)
                        {
                            n.left = new Node(x);
                            stop = true;
                        }
                        n = n.left;
                    }
                    else
                    {
                        if (n.right == null)
                        {
                            n.right = new Node(x);
                            stop = true;
                        }
                        n = n.right;
                    }
                }
            }
        }
        public Node findNode(int x)
        {
            Node n = startNode;
            while (true)
            {
                if (n.value == x)
                    return n;
                else if (n.value > x)
                {
                    if (n.left == null)
                        throw new NodeNotFoundException("Kein Knoten mit dem Wert: " + x + " gefunde");
                    n = n.left;
                }
                else
                {
                    if (n.right == null)
                        throw new NodeNotFoundException("Kein Knoten mit dem Wert: " + x + " gefunde");
                    n = n.right;
                }
            }
        }
    }


}

