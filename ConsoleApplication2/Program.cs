﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation
{
    class Program
    {
        static void Main(string[] args)
        {
            A a = new A(1);
            C c = new C(2,1);
           
            a.addOne();
            a.print();
            c.addOne();
            c.print();

            OutRef test = new OutRef();
            
            int y = 3;
            test.addOne(ref y);
            Console.WriteLine(" Y:" + y);

            int[] x;
            test.fillArray(out x,6);


            foreach (int i in x)
            {
                Console.WriteLine(i);
            }


            Dummy dummy = new Dummy();
            dummy.b = y;
            y = dummy.a;

            Console.WriteLine("A: " + dummy.a + " B: " + dummy.b + " Y:" + y);


            Vec4<int> n = new Vec4<int>(1,2,3,4);


            //Vec3 v1 = new Vec3(1, 2, 3);
            //Vec3 v2 = new Vec3(1, 2, 3);


            //Console.WriteLine(v1 + v2);
            //Console.WriteLine(v1 - v2);


            Vec2 v1 = new Vec2();  // x: 1, y: 1
            Vec2 v2 = new Vec2(y: 2); // x: 1, y: 2
            Vec2 v3 = new Vec2(3, 2); // x: 2, y: 3

            BinTree tree = new BinTree();
            tree.addNode(3);
            tree.addNode(6);
            try
            {
                tree.findNode(6);
                tree.findNode(2);
            }
            catch (NodeNotFoundException e)
            {
                Console.WriteLine(e.Message);
            }


            Console.WriteLine(n);
            Console.ReadLine();
        }
    }
}
