﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation
{
    class Vec3
    {
        private int x;
        private int y;
        private int z;

        public Vec3()
        {
            this.x = 0;
            this.y = 0;
            this.z = 0;
        }

        public Vec3(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            Presentation.B b = new Presentation.B(1);
        }


        public static Vec3 operator +(Vec3 v1, Vec3 v2)
        {
            Vec3 v3 = new Vec3();
            v3.x = v1.x + v2.x;
            v3.y = v1.y + v2.y;
            v3.z = v1.z + v2.z;
            return v3;
        }

        public static Vec3 operator -(Vec3 v1, Vec3 v2)
        {
            Vec3 v3 = new Vec3();
            v3.x = v1.x - v2.x;
            v3.y = v1.y - v2.y;
            v3.z = v1.z - v2.z;
            return v3;
        }

        public override string ToString()
        {
            return "Vec3: x: " + x + "y: " + y + " z:" + z;
        }
    }
}
