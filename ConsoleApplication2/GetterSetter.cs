﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation
{
    class Dummy
    {
        public int a {get; private set; }
        private int _b;
        public int b
        {
            get{
                return _b;
            }
            set
            {
                _b = value;
                a = value;
            }

        }
    }
}
