﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation
{
    interface IMyclass 
    {
        void addOne();
        void print();
    }

    class A : IMyclass 
    {
        private int a;

        public A(int a)
        {
            this.a = a;
        }

        public virtual void addOne() 
        {
            a ++;
        }
        public virtual void print()
        {
            Console.WriteLine("A: " + a);
        }
    }

    sealed class B : IMyclass
    {
        private int a;

        public B(int a)
        {
            this.a = a;
        }

        public void addOne()
        {
            a += 1;
        }

        public void print()
        {
            Console.WriteLine("A: " + a);
        }
    }

    class C : A
    {
        private int b;

        public C(int a, int b):base(a)
        {
            this.b = b;
        }
        public override void addOne()
        {
            base.addOne();
            b ++;
        }

        public override void print()
        {
            base.print();
            Console.WriteLine("B: " + b);
        }
    }

    abstract class D : IMyclass
    {
        protected int a;


        public void addOne()
        {
            a += 1;
        }

        public abstract void print();

    }

    class F : D
    {

        public F(int a)
        {
            this.a = a;
        }

        public override void print() 
        {
            Console.WriteLine("A: " + a);
        }
    }
}
